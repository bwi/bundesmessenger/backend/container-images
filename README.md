<!-- markdownlint-disable -->
<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/logo.png" alt="BundesMessenger Logo" width="256" height="256">
</div>

<div align="center">
  <h2 align="center">BundesMessenger</h2>
</div>
<div align="center">
  Der souveräne Messenger für Deutschland
</div>
<!-- markdownlint-enable -->

# BundesMessenger Container Registry

Die BundesMessenger Container Registry ist Bestandteil einer Sammlung von
Repositories zum BundesMessenger.
Allgemeine Informationen zum Projekt befinden sich im übergeordneten
[**BundesMessenger Repository**](https://gitlab.opencode.de/bwi/bundesmessenger/info).

Dieses Repository stellt die Container Images in der
[OpenCoDE GitLab Container-Registry](https://gitlab.opencode.de/bwi/bundesmessenger/backend/container-images/container_registry),
die für die Bereitstellung des BundesMessenger benötigt werden, zur Verfügung.

- zur [Container-Registry](https://gitlab.opencode.de/bwi/bundesmessenger/backend/container-images/container_registry)

# BWI Container Factory

Alle Container Images für das BundesMessenger Container Deployment werden
_from the scratch_ durch die
[BWI Container Factory](https://gitlab.opencode.de/bwi/bundesmessenger/backend/container-factory)
erstellt und kontinuierlich aktualisiert. Wir verwenden bewusst keine Images von
Docker Hub, um unsere Images lizenzkonform und aktualisierbar bereitzustellen.

Basis für alle BundesMessenger Container Images ist Ubuntu Jammy

Wesentliche Vorteile unserer Images sind:

- lizenzkonform, durch Integration einer SBOM und Bereitstellung der Sourcen
  der Systempackages und Anwendungsmodule
- Der Buildprozess findet in einem rootless Container statt
  (Principle of Least Privilege)
- Immutable Tags für eindeutige Versionierung (Tag zeigt immer auf das gleiche
  Container Image)
- Die Images ermöglichen Repeatable Deployments durch die Bereitstellung der
  Immutable Tags (Erhöhung der Sicherheit)
- Rolling Tags für Sicherheitsupdates
- Images werden für den rootless Einsatz vorbereitet (z.B. bei nginx: Verwendung
  von Ports > 1024)
- Reproducible builds für eine Überprüfbarkeit der Basis-Layer gegen die
  offiziellen Snapshots von debian
- Härtung durch Entfernen von Paketmanagern und unnötigen Tools

Folgende Images werden durch das Buildsystem aktuell unterstützt:

- [ClamAV](https://www.clamav.net/)
- [Matrix Content Scanner](https://github.com/vector-im/matrix-content-scanner-python)
- [nginx](https://www.nginx.com/)
- [Redis](https://redis.io/)
- [Sygnal](https://github.com/matrix-org/sygnal)
- [Synapse](https://github.com/element-hq/synapse)
- [Synapse Admin](https://github.com/Awesome-Technologies/synapse-admin)
- [KubeCtl](https://github.com/kubernetes/kubectl)
- icap
- wget

## Was heißt lizenzkonform?

Die Images enthalten sowohl eine SBOM mit allen Infos zu den genutzten Lizenzen
in dem jeweiligen Image, als auch den kompletten Source Code.
Wir haben uns dabei an die Empfehlungen vom [OSADL](https://osadl.org) gehalten.

Weitere Infos zum Thema lizenzkonforme Container Images findet ihr
bei [OSADL](https://www.osadl.org/COOL-2021-09-Compact-OSADL-Online-Le.cool-2021-09.0.html)

## Versionierung der Images

Das Namensschema und Hinweise zur Versionierung finden sich in der Readme der
[Container Images](https://gitlab.opencode.de/bwi/bundesmessenger/backend/container-factory/-/blob/main/docs/buildsystem_operation.md#versioning-and-tagging-system).

# Kontakt und Austausch

Für den Austausch zum BundesMessenger haben wir einen
[Matrix Raum](https://matrix.to/#/#opencodebum:matrix.org) erstellt.

<!-- markdownlint-disable -->
<div align="center">
  <img src="https://gitlab.opencode.de/bwi/bundesmessenger/info/-/raw/main/images/qr_matrix_room.png" alt="QR Code Matrix">
</div>
<!-- markdownlint-enable -->

Kein Matrix Client zur Hand, dann auch gerne über unser
[Email Postfach](mailto:bundesmessenger@bwi.de).

Wir freuen uns auf den Austausch.
